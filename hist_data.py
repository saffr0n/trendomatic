import argparse
from ib_insync import *
from datetime import datetime
from tempfile import mkstemp
import os

import pandas as pd
import boto3




TODAY = datetime.today().strftime("%Y-%m-%d")

NASDAQ = 'https://www.nasdaq.com/screening/companies-by-industry.aspx?sortname=marketcap&sorttype=1&exchange=NASDAQ&render=download'

ib = IB()

BUCKET_NAME = os.getenv("AWS_S3_BUCKET_NAME")
PREFIX = os.getenv("AWS_S3_PREFIX")
AWS_S3_ENDPOINT = os.environ.get('AWS_S3_ENDPOINT')
TWS_IP = os.environ.get('IB_TWS_IP')

FILE_NAME = PREFIX + '/' + TODAY + '/data.csv'

s3 = boto3.client('s3', endpoint_url=AWS_S3_ENDPOINT)


def to_float(val):
    magn = dict(M=6, B=9)

    if isinstance(val, str):
        if val[-1] in magn:
            num, magnitude = val[1:-1], val[-1]
            return float(num) * 10 ** magn[magnitude]
        return float(val[1:])
    return float(val)


def get_companies(link):
    companies = pd.read_csv(link).sort_values('MarketCap', ascending=False).reset_index(drop=True)
    companies = companies[~companies.Name.str.contains('[\s,\-]ADR', case=True, regex=True)][:2000]
    companies.MarketCap = companies.MarketCap.apply(to_float)
    return companies


def extract(df):

    df['dt'] = pd.to_datetime(df.date, format='%m/%d/%Y').astype('M8[ns]')

    for col in ('open', 'high', 'low', 'close'):
        df[col] = df[col].astype('float32')

    return df[['dt', 'open', 'high', 'low', 'close', 'volume', 'symbol']]


def save_historical_data(conn, companies):

    symbols = companies['Symbol'][:3]

    contracts = []

    for symbol in symbols:

        contracts.append(Stock(symbol, 'SMART', 'USD'))

    qcontracts = conn.qualifyContracts(*contracts)

    contracts = pd.DataFrame()

    for contract in qcontracts:

        bars = conn.reqHistoricalData(
            contract,
            endDateTime='',
            durationStr='1 Y',
            barSizeSetting='1 day',
            whatToShow='TRADES',
            useRTH=False,
            formatDate=2)

        df = util.df(bars)
        df['symbol'] = contract.symbol
        contracts = pd.concat([df, contracts], ignore_index=True)

    _, tmp_file = mkstemp()
    extract(contracts).to_parquet(tmp_file)

    s3.put_object(Bucket=BUCKET_NAME, Key=FILE_NAME, Body=open(tmp_file, 'rb'))
    os.unlink(tmp_file)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Get Historical Data')
    parser.add_argument('-i', '--ip',
                        help='IP-address of server',
                        default='127.0.0.1')

    parser.add_argument('-p', '--port',
                        help='Port number to connect to server',
                        default=7497,
                        type=int)

    parser.add_argument('-l', '--link',
                        help='URL to the companies list',
                        default=NASDAQ,
                        type=str)

    parser.add_argument('-c', '--client',
                        help='client id',
                        default=2,
                        type=int)

    arguments = parser.parse_args()

    companies = get_companies(arguments.link)
    conn = ib.connect(arguments.ip, arguments.port, arguments.client)

    parser.set_defaults(callback=save_historical_data(conn, companies))

    save_historical_data(conn, companies)