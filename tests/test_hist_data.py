from datetime import datetime, date
import io
import os

import pandas as pd
import pytest
from hamcrest import assert_that
from ib_insync import BarData
import boto3


@pytest.yield_fixture
def prog():
    import hist_data

    s3_con = boto3.resource('s3',
                            endpoint_url=hist_data.AWS_S3_ENDPOINT,
                            region_name='us-east-1')    # эмуляция подключения к s3

    s3_con.create_bucket(Bucket=hist_data.BUCKET_NAME)       # создаём бакет

    yield {'prog':hist_data,
           's3_con':s3_con}


def test_to_float(prog):
    assert isinstance(prog['prog'].to_float('22'), float)
    assert isinstance(prog['prog'].to_float(22.), float)
    assert isinstance(prog['prog'].to_float(22), float)


def test_get_companies(prog):
    link = prog['prog'].NASDAQ
    result = prog['prog'].get_companies(link)
    assert result.iloc[0]['MarketCap'] > result.iloc[-1]['MarketCap']    # проверка порядка
    assert len(result.loc[result.Name.str.contains('[\s, -]ADR', case=True, regex=True)]) == 0  # проверка регулярки
    assert len(result) == 2000    # проверка количества компаний


def test_save_historical_data(prog):

    def gen_hist_data_list(days):
        """
        Преобразуем датафрейм в генератор объектов BarData
        """
        for day in range(days):
            yield BarData(date=datetime.today().strftime("%m/%d/%Y"),
                          open=3.18,
                          high=3.18,
                          low=3.08,
                          close=3.08,
                          volume=1870906.0)

    def total_days_in_year():
        """
        Точное количесвто прошедших дней за год
        """
        now = datetime.now().date()
        ly = date(now.year - 1, now.month, now.day)

        delta = now - ly
        return delta.days

    total_days = total_days_in_year()

    class Conn(object):

        def reqHistoricalData(self, *args, **kwargs):
            return list(gen_hist_data_list(total_days))

        def qualifyContracts(self, *args):
            return args

    companies = prog['prog'].get_companies(prog['prog'].NASDAQ)

    ib_con = Conn()

    prog['prog'].save_historical_data(ib_con, companies)      # запуск сбора и записи данных

    result = prog['s3_con'].Object(prog['prog'].BUCKET_NAME, prog['prog'].FILE_NAME).get()['Body'].read()
    result = pd.read_parquet(io.BytesIO(result))

    assert_that(total_days, len(result.index))              # проверка на количество данных по дням
    assert_that(total_days, result.count(axis=1))           # проверка целостности данных (на наличие пустых)


def test_extract(prog):
    """
    Проверка наличия столбцов и функции extract
    """
    result = prog['s3_con'].Object(prog['prog'].BUCKET_NAME, prog['prog'].FILE_NAME).get()['Body'].read()
    result = pd.read_parquet(io.BytesIO(result))

    expected = ['dt', 'open', 'high', 'low', 'close', 'volume', 'symbol']
    assert False not in [x in result for x in expected]


def test_data_types(prog):
    """
    Проверяем типы данных
    """
    result = prog['s3_con'].Object(prog['prog'].BUCKET_NAME, prog['prog'].FILE_NAME).get()['Body'].read()
    result = pd.read_parquet(io.BytesIO(result))

    vals = list(next(result.iterrows())[1]\
                    [['open', 'hight', 'low', 'close', 'volume']])      # берём только нужные столбцы
    assert False not in [type(x) == float for x in vals]                # запускаем теcт по ним
