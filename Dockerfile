FROM amazonlinux:latest
MAINTAINER RBeneficious Team <tech@beneficous.com>

ENV PYTHONPATH=/ \
    AWS_DEFAULT_REGION="us-east-1" \
    TZ="US/Eastern" \
    AWS_S3_ENDPOINT="http://localhost:4572" \
    AWS_S3_BUCKET_NAME="DevBucket" \
    AWS_S3_PREFIX="overnight" \
    AWS_SECRET_ACCESS_KEY=foobar \
    AWS_ACCESS_KEY_ID=foobar

ARG buildreq="gcc-c++ gcc-gfortran unzip python36-devel  pcre-devel xz-devel bzip2-devel zlib-devel libicu-devel"
ARG twsapi_version="973.06"

COPY requirements.txt /requirements.txt

RUN yum -y install python36 util-linux libgfortran-6.4.1-1.45.amzn1 $buildreq \
    && touch /etc/redhat-release \
    && curl -o twsapi.zip https://interactivebrokers.github.io/downloads/twsapi_macunix.$twsapi_version.zip \
    && unzip twsapi.zip -d twsapi \
    && pip-3.6 install twsapi/IBJts/source/pythonclient/ \
    && rm -f twsapi.zip \
    && rm -rf twsapi \
    && rm -f /etc/redhat-release \
    && pip-3.6 install --no-cache -r /requirements.txt \
    && yum autoremove -y $buildreq \
    && yum clean all \
    && ./env.sh

COPY . .
